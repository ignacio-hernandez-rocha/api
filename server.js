//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json')

console.log('todo list RESTful API server started on: ' + port);

app.get("/", (req, res) => {
  //res.send("Hola mundo desde node.js");
  res.sendFile(path.join(__dirname, "index.html"));
});

app.post("/", (req, res) => {
  res.send("Hemos recibido tu peticion post");
});

app.delete("/", (req, res) => {
  res.send("Hemos recibido tu peticion delete");
});

app.put("/", (req, res) => {
  res.send("Hemos recibido tu peticion put");
});

app.get("/clientes", (req, res) => {
  res.send("Aqui tiene a los clientes");
});

app.get("/clientes/:idcliente", (req, res) => {
  res.send("Aqui tiene al cliente: " + req.params.idcliente);
});

app.get("/v1/movimientos", (req, res) => {
  res.sendFile(path.join(__dirname, "movimientosv1.json"));
});


app.get("/v2/movimientos", (req, res) => {
  res.json(movimientosJSONv2);
});

app.get("/v2/movimientos/:id", (req, res) => {
  console.log(req.params.id);
  res.json(movimientosJSONv2[req.params.id - 1]);
});

app.get("/v2/movimientos/:id/:nombre", (req, res) => {
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
});

//query parameters
app.get("/v2/movimientosq", (req, res) => {
  console.log(req.query);
  res.send("peticion con query recibida y cambiada: " + JSON.stringify(req.query));
});
